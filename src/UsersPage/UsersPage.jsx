import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class UsersPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAllUsers());
    }

    handleDeleteUserNoAuth(id) {
        return (e) => this.props.dispatch(userActions.deleteNoAuth(id));
    }

    render() {
        const { users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h3>All registered users:</h3>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                  <span>Total No Of Users: {users.items.length}</span>
                }
                {users.items &&
                    <ul>
                        {users.items.map((user, index) =>
                            <li key={user.id}>
                                <Link to={'user/'+user.username}>{user.username}</Link>
                                {' | ' +user.firstName + ' ' + user.lastName}
                                {
                                    user.deleting ? <em> - Deleting...</em>
                                    : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                    : <span> - <a onClick={() => {if(confirm('Delete the item?')) {this.handleDeleteUserNoAuth(user.id)};}}>Delete</a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
                <Link to="/login" className="btn btn-primary">Login</Link>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(UsersPage);
export { connectedHomePage as UsersPage };
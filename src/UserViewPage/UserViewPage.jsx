import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class UserViewPage extends React.Component {
    componentDidMount() {
        console.log('******************88', this.props.match.id);
        // this.props.dispatch(userActions.getUser());
    }

    render() {
        const { user } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h3>User:</h3>

                <Link to="/login" className="btn btn-primary">Login</Link>
                <Link to="/users" className="btn btn-link">Users</Link>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    };
}

const connectedHomePage = connect(mapStateToProps)(UserViewPage);
export { connectedHomePage as UserViewPage };
